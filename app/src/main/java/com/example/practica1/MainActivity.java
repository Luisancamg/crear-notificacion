package com.example.practica1;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica1.Api.Api;
import com.example.practica1.Api.Servicios.ServicioPeticion;
import com.example.practica1.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private TextView Texto1;
    private TextView Texto2;

    private Typeface Sunday;
    private Typeface SundayB;


    private TextView textView4;
    private Button button2;
    private EditText editText6;
    private EditText editText4;
    private EditText editText5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String fuente1 = "fuentes/Sunday.otf";
        this.Sunday = Typeface.createFromAsset(getAssets(),fuente1);

        Texto1 = (TextView) findViewById(R.id.textView3);
        Texto2 = (TextView) findViewById(R.id.textView4);

        textView4 = (TextView) findViewById(R.id.textView4);

        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Siguiente = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(Siguiente);
            }
        });

        button2 = (Button) findViewById(R.id.button2);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        editText6 = (EditText) findViewById(R.id.editText6);


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(editText6.getText().toString()))
                editText6.setError("Este campo no puede estar vacio");
                if(TextUtils.isEmpty(editText4.getText().toString()))
                editText4.setError("Este campo no puede estar vacio");
                if(TextUtils.isEmpty(editText5.getText().toString()))
                editText5.setError("Este campo no puede estar vacio");
                else if(!TextUtils.equals(editText4.getText(),editText5.getText()))
                    editText5.setError("La Contraseña no coincide");
                else{

                    ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                    Call<Registro_Usuario> registrarCall = service.registrarUsuario(editText6.getText().toString(), editText4.getText().toString());
                    registrarCall.enqueue(new Callback<Registro_Usuario>() {
                        @Override
                        public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                            Registro_Usuario peticion = response.body();
                            if (response.body()==null){
                                Toast.makeText(MainActivity.this, "Ocurrio un error, intentalo mas tarde", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(peticion.estado == "true"){
                                startActivity(new Intent(MainActivity.this, Main2Activity.class));
                                Toast.makeText(MainActivity.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(MainActivity.this, peticion.detalle, Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();

                        }



                    });
                }
            }
        });
    }



}
